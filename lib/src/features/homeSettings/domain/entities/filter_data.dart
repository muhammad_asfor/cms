import 'package:json_annotation/json_annotation.dart';

part 'filter_data.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class FilterData {
  final int? priceMax;
  final int? priceMin;

  FilterData({
    required this.priceMax,
    required this.priceMin,
  });

  factory FilterData.fromJson(json) => _$FilterDataFromJson(json);
  toJson() => _$FilterDataToJson(this);
}
