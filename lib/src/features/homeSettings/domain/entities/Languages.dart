import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'Languages.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class Languages {
  final int? id;
  final List<String>? flag;
  final String? slug;
  final bool? isActive;
  final String? coverImage;
  final String? title;
  factory Languages.fromJson(json) => _$LanguagesFromJson(json);
  toJson() => _$LanguagesToJson(this);
  Languages(
      {this.id,
      this.flag,
      this.slug,
      this.isActive,
      this.coverImage,
      this.title});

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Languages && other.slug == slug;
  }

  @override
  int get hashCode {
    return slug.hashCode;
  }
}
