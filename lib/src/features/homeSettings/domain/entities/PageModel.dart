import 'package:json_annotation/json_annotation.dart';
part 'PageModel.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class PageModel {
  final int id;
  final String title;
  final String? description;
  final String? coverImage;
  factory PageModel.fromJson(json) => _$PageModelFromJson(json);
  toJson() => _$PageModelToJson(this);
  static List<PageModel> fromJsonList(List json) {
    return json.map((e) => PageModel.fromJson(e)).toList();
  }
  PageModel({required this.id, required this.title, this.description,this.coverImage});
}
