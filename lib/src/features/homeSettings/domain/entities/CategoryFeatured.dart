import 'package:json_annotation/json_annotation.dart';
import 'package:progiom_cms/src/features/core/SharedEntities/BaseListResponse.dart';
import 'package:progiom_cms/src/features/core/SharedEntities/Product.dart';

part 'CategoryFeatured.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class CategoryFeatured {
  final int id;
  final String title;
  final String coverImage;
  @JsonKey(fromJson: baseFromJson)
  final BaseListResponse<Product> posts;

  CategoryFeatured({
    required this.id,
    required this.posts,
    required this.title,
    required this.coverImage
  });

  factory CategoryFeatured.fromJson(json) => _$CategoryFeaturedFromJson(json);
  toJson() => _$CategoryFeaturedToJson(this);

  static baseFromJson(json) {
    return BaseListResponse<Product>.fromJson(json, (data) => Product.fromJson(data));
  }
}
