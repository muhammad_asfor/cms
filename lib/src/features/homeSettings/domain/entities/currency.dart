import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'currency.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class Currency {
  final int? id;
  final String? code;
  final String? title;
  final String? symbol;
  factory Currency.fromJson(json) => _$CurrencyFromJson(json);
  toJson() => _$CurrencyToJson(this);
  Currency(
      {this.id,
        this.code,
        this.title,
        this.symbol,
      });
}
