// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'CategoryFeatured.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CategoryFeatured _$CategoryFeaturedFromJson(Map<String, dynamic> json) =>
    CategoryFeatured(
      id: json['id'] as int,
      posts: CategoryFeatured.baseFromJson(json['posts']),
      title: json['title'] as String,
      coverImage: json['cover_image'] as String,
    );

Map<String, dynamic> _$CategoryFeaturedToJson(CategoryFeatured instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'cover_image': instance.coverImage,
      'posts': instance.posts,
    };
