import 'package:dartz/dartz.dart';
import 'package:progiom_cms/core.dart';

import 'package:progiom_cms/src/features/homeSettings/domain/repositories/settings_repository.dart';

import '../../homeSettings.dart';

class GetCategoryDataUseCase
    extends UseCase<Category, GetCategoryDataUseCaseParams> {
  final SettingsRepository repository;

  GetCategoryDataUseCase(
    this.repository,
  );

  @override
  Future<Either<Failure, Category>> call(
      GetCategoryDataUseCaseParams params) async {
    return await repository.getCategoryData(
        categoryId: params.id);
  }
}

class GetCategoryDataUseCaseParams {
  final String id;

  GetCategoryDataUseCaseParams({required this.id});
}
