import 'package:dartz/dartz.dart';

import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:progiom_cms/src/features/homeSettings/domain/repositories/settings_repository.dart';

import '../../homeSettings.dart';

class GetSettingsUseCase
    extends UseCase<SettingsModel, GetSettingsUseCaseParams> {
  final SettingsRepository repository;

  GetSettingsUseCase(
    this.repository,
  );

  @override
  Future<Either<Failure, SettingsModel>> call(
      GetSettingsUseCaseParams params) async {
    return await repository.getSettings(
        osType: params.osType,
        osVersion: params.osVersion,
        appVersion: params.appVersion,
        countryId: params.countryId,
        cityId: params.cityId,
        stateId: params.stateId,
        lan: params.lan);
  }
}

class GetSettingsUseCaseParams {
  final String osType;
  final String osVersion;
  final String appVersion;
  final int? countryId;
  final String? lan;
  final int? cityId;
  final int? stateId;
  GetSettingsUseCaseParams(
      {required this.osType,
      required this.osVersion,
      required this.appVersion,
      this.countryId,
      this.cityId,
      this.stateId,
      this.lan});
}
