import 'package:dartz/dartz.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/src/features/core/util/error/failures.dart';
import 'package:progiom_cms/src/features/homeSettings/data/datasources/HomeSettingsApi.dart';
import '../../homeSettings.dart';

abstract class SettingsRepository {
  Future<Either<Failure, SettingsModel>> getSettings(
      {required String osType,
      required String osVersion,
      required String appVersion,
      int? countryId,
      int? cityId,
      int? stateId,
      String? lan});

  Future<Either<Failure, List<Category>>> getCategories();
  Future<Either<Failure, List<Category>>> getLsitCategories(
      SearchCategoryParams searchCategoryParams);
  Future<Either<Failure, Category>> getCategoryData({
    required String categoryId,
  });

  Future<Either<Failure, List<PageModel>>> getPages();

  Future<Either<Failure, PageModel>> getPageDetails({
    required int id,
  });
  Future<Either<Failure, List<Slide>>> getMobileSlides();
  Future<Either<Failure, List<Slide>>> getMobileSlidesEnd();

  Future<Either<Failure, Map>> getPreferences();
  Future<Either<Failure, bool>> updateNewCurrency();
}
