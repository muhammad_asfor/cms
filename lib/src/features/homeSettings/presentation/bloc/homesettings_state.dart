part of 'homesettings_bloc.dart';

@immutable
abstract class HomesettingsState {}

class HomesettingsInitial extends HomesettingsState {}

class HomeSettingsReady extends HomesettingsState {
  final PackageInfo packageInfo;
  final List<dynamic>? dynamicFilters;
  HomeSettingsReady(this.packageInfo, {this.dynamicFilters});
}

class ErrorInGettingSettings extends HomesettingsState {
  final String error;
  ErrorInGettingSettings(this.error);
}

class LoadingSettings extends HomesettingsState {}
