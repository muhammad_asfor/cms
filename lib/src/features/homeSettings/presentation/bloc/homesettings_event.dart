part of 'homesettings_bloc.dart';

@immutable
abstract class HomesettingsEvent {}

class GetSettings extends HomesettingsEvent {
  final int? countryId;
  final int? cityId;
  final int? stateId;
  final String? lan;
  GetSettings({this.countryId, this.cityId, this.stateId, this.lan});
}

class AddDynamicFields extends HomesettingsEvent {
  final String parentId;
  final String categoryId;

  AddDynamicFields(this.parentId, this.categoryId);
}

class ChangeDynamicValues extends HomesettingsEvent {
  final List<dynamic> dynamicValues;

  ChangeDynamicValues(this.dynamicValues);
}

class UpdateNewCurrency extends HomesettingsEvent {}
