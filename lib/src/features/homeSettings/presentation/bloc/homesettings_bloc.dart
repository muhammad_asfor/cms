import 'dart:async';
import 'dart:io';
import 'package:bloc/bloc.dart';

import 'package:meta/meta.dart';
import 'package:package_info_plus/package_info_plus.dart';

import 'package:device_info/device_info.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/src/features/auth/data/datasources/AuthSharedPref.dart';
import 'package:progiom_cms/src/features/homeSettings/domain/usecases/get_dynamic_fields.dart';
import 'package:progiom_cms/src/features/homeSettings/domain/usecases/update_new_currency.dart';
import '../../../../../Getit_instance.dart';
import '../../homeSettings.dart';

import 'package:progiom_cms/src/features/core/util/ios_version.dart';
import 'package:progiom_cms/src/features/homeSettings/domain/usecases/get_settings.dart';

part 'homesettings_event.dart';

part 'homesettings_state.dart';

class HomesettingsBloc extends Bloc<HomesettingsEvent, HomesettingsState> {
  HomesettingsBloc() : super(HomesettingsInitial()) {
    on<GetSettings>(_onGetSettings);
    on<AddDynamicFields>(_onAddDynamicFields);
    on<UpdateNewCurrency>(_onUpdateNewCurrency);
    on<ChangeDynamicValues>(_onChangeDynamicValues);
  }

  SettingsModel? settings;
  List<dynamic> filterValues = [];
  String? token;

  _onGetSettings(GetSettings event, Emitter<HomesettingsState> emit) async {
    emit(LoadingSettings());
    var packageInfo = await PackageInfo.fromPlatform();

    bool isIos = Platform.isIOS;
    var appVersion =
        isIos ? getIosVersionNumber(packageInfo) : packageInfo.version;

    String osVersion;
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (!isIos) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      osVersion = androidInfo.version.sdkInt.toString();
    } else {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      osVersion = iosInfo.systemVersion;
    }

    final GetSettingsUseCase getSettings = GetSettingsUseCase(sl());
    final result = await getSettings.call(GetSettingsUseCaseParams(
        osType: isIos ? "ios" : "android",
        osVersion: osVersion,
        appVersion: appVersion,
        countryId: event.countryId,
        lan: event.lan));
    token = await sl<AuthSharedPrefs>().getUserToken();
    result.fold((l) {
      emit(ErrorInGettingSettings(l.errorMessage));
    }, (r) {
      settings = r;
      emit(HomeSettingsReady(packageInfo));
    });
  }

  _onAddDynamicFields(
      AddDynamicFields event, Emitter<HomesettingsState> emit) async {
    var packageInfo = await PackageInfo.fromPlatform();
    emit(LoadingSettings());
    final GetCategoryDataUseCase getCategoryData = GetCategoryDataUseCase(sl());
    final result = await getCategoryData
        .call(GetCategoryDataUseCaseParams(id: event.categoryId));
    result.fold((l) {
      emit(ErrorInGettingSettings(l.errorMessage));
    }, (r) {
      List<dynamic> filterValues = [];
      if (event.parentId == event.categoryId) {
        settings!.categories!
            .firstWhere((element) => element.id.toString() == event.parentId)
            .dynamicFilterFields = r.dynamicFilterFields;
      } else {
        settings!.categories!
            .firstWhere((element) => element.id.toString() == event.parentId)
            .subCategories!
            .firstWhere((element) => element.id.toString() == event.categoryId)
            .dynamicFilterFields = r.dynamicFilterFields;
      }
      for (var e in r.dynamicFilterFields!) {
        if (e.type == 'select' || e.type == 'radio') {
          filterValues.add(null);
        } else {
          filterValues.add([]);
        }
      }
      print('filter values $filterValues');
      emit(HomeSettingsReady(packageInfo, dynamicFilters: filterValues));
    });
  }

  _onUpdateNewCurrency(
      UpdateNewCurrency event, Emitter<HomesettingsState> emit) async {
    emit(LoadingSettings());
    final UpdateNewCurrencyUseCase updateNewCurrencyUseCase =
        UpdateNewCurrencyUseCase(sl());
    final result = await updateNewCurrencyUseCase.call(NoParams());
    result.fold((l) {
      emit(ErrorInGettingSettings(l.errorMessage));
    }, (r) {
      print('doneee');
      add(GetSettings());
    });
  }

  _onChangeDynamicValues(
      ChangeDynamicValues event, Emitter<HomesettingsState> emit) async {
    var packageInfo = await PackageInfo.fromPlatform();
    filterValues = event.dynamicValues;
    emit(HomeSettingsReady(packageInfo, dynamicFilters: filterValues));
  }
}
