import 'package:dio/dio.dart';
import 'package:progiom_cms/core.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../../Getit_instance.dart';
import '../../../../../auth.dart';
import '../../homeSettings.dart';

class HomeSettingsApi {
  HomeSettingsApi();

  Future<bool> changeCurrencyInHeader() {
    sl<Dio>()
        .interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options, r) {
      options.headers["currency"] =
          sl<SharedPreferences>().getString(AuthConstants.Currency) ?? "USD";

      return r.next(options);
    }));
    return Future.value(true);
  }

  Future<SettingsModel> getSettings(
      String osType, String osVersion, String appVersion,
      {int? countryId, int? cityId, int? stateId, String? lan}) async {
    try {
      final response = await sl<Dio>().get('/api',
          options: Options(
              headers: countryId == null
                  ? {
                      "user-app-version": appVersion,
                      "user-os-version": osVersion,
                      "Accept-Language": lan ??
                          sl<SharedPreferences>()
                              .getString(AuthConstants.Language) ??
                          "ar",
                    }
                  : {
                      "user-app-version": appVersion,
                      "user-os-version": osVersion,
                      "country-id":
                          sl<SharedPreferences>().getString("country") ?? "",
                      "state-id":
                          sl<SharedPreferences>().getString("state") ?? "",
                      "city-id":
                          sl<SharedPreferences>().getString("city") ?? "",
                      "Accept-Language": lan ??
                          sl<SharedPreferences>()
                              .getString(AuthConstants.Language) ??
                          "ar",
                    }));

      final SettingsModel model = SettingsModel.fromJson(response.data);
      return model;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      print(e.toString());
      throw ServerException(e.toString());
    }
  }

  Future<List<Category>> getCategories() async {
    try {
      final response = await sl<Dio>().get('/api/categories');

      final List<Category> model = Category.fromJsonList(response.data['data']);
      return model;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      print(e.toString());
      throw ServerException(e.toString());
    }
  }

  Future<List<Slide>> getMobileSlides() async {
    try {
      final response =
          await sl<Dio>().get('/api/mobile_slides?type=additional_slider');

      final List<Slide> model = Slide.fromJsonList(response.data['data']);
      return model;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      print(e.toString());
      throw ServerException(e.toString());
    }
  }

  Future<List<Slide>> getMobileSlidesEnd() async {
    try {
      final response =
          await sl<Dio>().get('/api/mobile_slides?type=bottom_slider');

      final List<Slide> model = Slide.fromJsonList(response.data['data']);
      return model;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      print(e.toString());
      throw ServerException(e.toString());
    }
  }

  Future<List<Category>> getLsitCategories(
      SearchCategoryParams searchCategoryParams) async {
    try {
      final response = await sl<Dio>().get(
          '/api/categories?country_id=${sl<SharedPreferences>().getString("country") ?? ""}&state_id=${sl<SharedPreferences>().getString("state") ?? ""}&city_id=${sl<SharedPreferences>().getString("city") ?? ""}&q=${searchCategoryParams.q ?? ''}');

      final List<Category> model = Category.fromJsonList(response.data['data']);
      return model;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      print(e.toString());
      throw ServerException(e.toString());
    }
  }

  Future<Category> getCategoryData(String categoryId) async {
    try {
      final response = await sl<Dio>().get('/api/categories/$categoryId');

      final Category model = Category.fromJson(response.data);
      return model;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      print(e.toString());
      throw ServerException(e.toString());
    }
  }

  Future<PageModel> getPageDetails(int id) async {
    try {
      final response = await sl<Dio>().get(
        '/pages/$id',
      );

      final PageModel model = PageModel.fromJson(response.data);
      return model;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      print(e.toString());
      throw ServerException(e.toString());
    }
  }

  Future<List<PageModel>> getPages() async {
    try {
      return PageModel.fromJsonList(
          (await sl<Dio>().get("api/pages")).data["data"]);
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      print(e.toString());
      throw ServerException(e.toString());
    }
  }

  Future<Map> getPreferences() async {
    try {
      final response = await sl<Dio>().get(
        'api/settings',
      );

      return response.data['data'];
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      print(e.toString());
      throw ServerException(e.toString());
    }
  }
}

class SearchCategoryParams {
  final int? countryId;
  final int? stateId;
  final int? cityId;
  final String? q;

  SearchCategoryParams({this.countryId, this.stateId, this.cityId, this.q});
}
