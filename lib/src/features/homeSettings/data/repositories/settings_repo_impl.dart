import 'package:dartz/dartz.dart';
import 'package:progiom_cms/core.dart';

import 'package:progiom_cms/src/features/homeSettings/data/datasources/HomeSettingsApi.dart';
import 'package:progiom_cms/src/features/homeSettings/data/datasources/home_settings_local_data_source.dart';
import 'package:progiom_cms/src/features/homeSettings/domain/repositories/settings_repository.dart';

import '../../homeSettings.dart';

class SettingsRepositoryImpl implements SettingsRepository {
  final HomeSettingsApi homeSettingsApi;
  final HomeSettingsLocalDataSource homeSettingsLocalDataSource;

  SettingsRepositoryImpl(
      this.homeSettingsApi, this.homeSettingsLocalDataSource);

  @override
  Future<Either<Failure, SettingsModel>> getSettings(
      {required String osType,
      required String osVersion,
      required String appVersion,
      int? countryId,
      int? cityId,
      int? stateId,
      String? lan}) async {
    try {
      final result = await homeSettingsApi.getSettings(
          osType, osVersion, appVersion,
          lan: lan, countryId: countryId, cityId: cityId, stateId: stateId);
      getPreferences(); // get preferences after home
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, List<PageModel>>> getPages() async {
    try {
      final result = await homeSettingsApi.getPages();
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, PageModel>> getPageDetails({required int id}) async {
    try {
      final result = await homeSettingsApi.getPageDetails(id);
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, bool>> updateNewCurrency() async {
    try {
      final result = await homeSettingsApi.changeCurrencyInHeader();
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, List<Category>>> getCategories() async {
    try {
      final result = await homeSettingsApi.getCategories();
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, List<Category>>> getLsitCategories(
      SearchCategoryParams searchCategoryParams) async {
    try {
      final result =
          await homeSettingsApi.getLsitCategories(searchCategoryParams);
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, Category>> getCategoryData(
      {required String categoryId}) async {
    try {
      final result = await homeSettingsApi.getCategoryData(categoryId);
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, Map>> getPreferences() async {
    try {
      final localResult = await homeSettingsLocalDataSource.getPreferences();
      if (localResult == null) {
        final result = await homeSettingsApi.getPreferences();

        CartNumber.instance.set(result["cart_items_count"] ?? 0);

        return Right(result);
      } else {
        CartNumber.instance.set(localResult["cart_items_count"] ?? 0);

        return Right(localResult);
      }
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, List<Slide>>> getMobileSlides() async {
    try {
      final result = await homeSettingsApi.getMobileSlides();
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, List<Slide>>> getMobileSlidesEnd() async {
    try {
      final result = await homeSettingsApi.getMobileSlidesEnd();
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }
}
