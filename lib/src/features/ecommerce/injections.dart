import 'package:progiom_cms/Getit_instance.dart';
import 'package:progiom_cms/src/features/ecommerce/data/datasources/Ecommerce_api.dart';
import 'package:progiom_cms/src/features/ecommerce/data/repositories/Ecommerce_repo_empl.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';

import 'data/datasources/EcommerceSharedPrefs.dart';

// depend on shredpreferences ans dio.
void initEcommerceInjections() async {
  sl.registerLazySingleton<EcommerceApi>(() => EcommerceApi());

  sl.registerLazySingleton<EcommerceSharedPrefs>(
      () => EcommerceSharedPrefs(sl()));

  sl.registerLazySingleton<EcommerceRepository>(
      () => EcommerceRepositoryImpl(sl(), sl()));
}
