import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/CartModel.dart';

import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';
import 'package:progiom_cms/src/features/homeSettings/homeSettings.dart';

class GetCart extends UseCase<CartModel, NoParams> {
  final EcommerceRepository repository;
  GetCart(this.repository);

  @override
  Future<Either<Failure, CartModel>> call(NoParams noParams) async {
    final result = await repository.getCart();
    CartNumber.instance.sync();
    return result;
  }
}
