import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';

class SetDefaultAddress extends UseCase<bool, SetDefaultAddressParams> {
  final EcommerceRepository repository;
  SetDefaultAddress(this.repository);

  @override
  Future<Either<Failure, bool>> call(SetDefaultAddressParams params) async {
    return await repository.setDefaultAddress(addressId: params.addressId);
  }
}

class SetDefaultAddressParams {
  final int addressId;

  SetDefaultAddressParams({
    required this.addressId,
  });
}
