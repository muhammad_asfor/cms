import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/Address.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';

class GetAddressDetails extends UseCase<Address, GetAddressDetailsParams> {
  final EcommerceRepository repository;
  GetAddressDetails(this.repository);

  @override
  Future<Either<Failure, Address>> call(GetAddressDetailsParams params) async {
    return await repository.getAddressDetails(params.id);
  }
}

class GetAddressDetailsParams {
  final int id;

  GetAddressDetailsParams({
    required this.id,
  });
}
