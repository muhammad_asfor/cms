import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';

class DeleteAddress extends UseCase<bool, DeleteAddressParams> {
  final EcommerceRepository repository;
  DeleteAddress(this.repository);

  @override
  Future<Either<Failure, bool>> call(DeleteAddressParams params) async {
    return await repository.deleteAddress(
      params.id,
    );
  }
}

class DeleteAddressParams {
  final int id;

  DeleteAddressParams({
    required this.id,
  });
}
