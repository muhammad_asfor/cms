import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/entities/Address.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';

class UpdateAddress extends UseCase<bool, UpdateAddressParams> {
  final EcommerceRepository repository;
  UpdateAddress(this.repository);

  @override
  Future<Either<Failure, bool>> call(UpdateAddressParams params) async {
    return await repository.updateAddress(
        cityId: params.cityId,
        countryId: params.countryId,
        name: params.name,
        id: params.id,
        description: params.description);
  }
}

class UpdateAddressParams {
  final int countryId;
  final int cityId;
  final String name;
  final String description;
  final int id;
  UpdateAddressParams({
    required this.cityId,
    required this.countryId,
    required this.name,
    required this.id,
    required this.description,
  });
}
