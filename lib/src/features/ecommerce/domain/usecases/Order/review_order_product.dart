import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';

import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';

class ReviewOrderProduct extends UseCase<bool, ReviewOrderProductParams> {
  final EcommerceRepository repository;
  ReviewOrderProduct(this.repository);

  @override
  Future<Either<Failure, bool>> call(ReviewOrderProductParams params) async {
    return await repository.reviewOrderProduct(
        itemId: params.itemId, comment: params.comment, rating: params.rating);
  }
}

class ReviewOrderProductParams {
  final int itemId;
  final int rating;
  final String comment;
  ReviewOrderProductParams(
      {required this.itemId, required this.comment, required this.rating});
}
