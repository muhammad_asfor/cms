import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';

class GetProducatsByCategory
    extends UseCase<List<Product>, ProducatsByCategoryParams> {
  final EcommerceRepository repository;

  GetProducatsByCategory(this.repository);

  @override
  Future<Either<Failure, List<Product>>> call(
      ProducatsByCategoryParams params) async {
    String queryParams = "";
    if (params.minPrice != null || params.maxPrice != null) {
      queryParams += "&price[min]=${params.minPrice ?? 0}";
      queryParams += "&price[max]=${params.maxPrice ?? 20000}";
    }
    if (params.orderColumn != null) {
      queryParams += "&order[column]=${params.orderColumn}";
    }
    if (params.orderDirection != null) {
      queryParams += "&order[dir]=${params.orderDirection}";
    }
    if (params.rating != null) {
      queryParams += "&rating=${params.rating}";
    }
    return await repository.getListProducts(
      endpoint: "/posts" +
          "?parent_id=" +
          params.parentId! +
          "&page=" +
          params.page.toString() +
          queryParams,
      filterValues: params.filterValues,
    );
  }
}

class ProducatsByCategoryParams {
  final int? page;
  final String? minPrice;
  final String? maxPrice;
  final String? orderColumn;
  final String? orderDirection;
  final int? categoryId;
  final int? rating;
  final String? parentId;
  final Map<int, dynamic>? filterValues;

  ProducatsByCategoryParams(
      {this.parentId,
      this.page,
      this.minPrice,
      this.maxPrice,
      this.orderColumn,
      this.orderDirection,
      this.categoryId,
      this.rating,
      this.filterValues});
}
