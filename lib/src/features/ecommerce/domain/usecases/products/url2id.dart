import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/ecommerce/domain/repositories/Ecommerce_repository.dart';

class Url2Id extends UseCase<String, Url2IdParams> {
  final EcommerceRepository repository;
  Url2Id(this.repository);

  @override
  Future<Either<Failure, String>> call(Url2IdParams params) async {
    return await repository.url2id(params.url);
  }
}

class Url2IdParams {
  final String url;

  Url2IdParams({
    required this.url,
  });
}
