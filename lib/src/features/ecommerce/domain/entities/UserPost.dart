class UserPost {
  int? id;
  int? parentId;
  String? title;
  String? description;
  String? videoUrl;
  String? shortDescription;
  String? categoryText;
  String? coverImage;
  List<String>? imagesData;
  List<String>? imagesBag;
  User? user;

  UserPost(
      {this.id,
      this.parentId,
      this.title,
      this.description,
      this.videoUrl,
      this.shortDescription,
      this.categoryText,
      this.coverImage,
      this.imagesData,
      this.imagesBag,
      this.user});

  UserPost.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    parentId = json['parent_id'];
    title = json['title'];
    description = json['description'];
    videoUrl = json['video_url'];
    shortDescription = json['short_description'];
    categoryText = json['category_text'];
    coverImage = json['cover_image'];
    imagesData = json['images_data'].cast<String>();
    imagesBag = json['images_bag'].cast<String>();
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['parent_id'] = this.parentId;
    data['title'] = this.title;
    data['description'] = this.description;
    data['video_url'] = this.videoUrl;
    data['short_description'] = this.shortDescription;
    data['category_text'] = this.categoryText;
    data['cover_image'] = this.coverImage;
    data['images_data'] = this.imagesData;
    data['images_bag'] = this.imagesBag;
    if (this.user != null) {
      data['user'] = this.user!.toJson();
    }
    return data;
  }
}

class User {
  int? id;
  String? name;
  String? email;
  String? countryCode;
  String? mobile;
  int? countryId;
  int? stateId;
  int? cityId;
  int? packageId;
  String? coverImage;
  bool? isTechnician;
  bool? hasPendingRequest;
  int? grade;
  bool? isVerified;
  String? countryText;
  String? stateText;
  String? cityText;

  User(
      {this.id,
      this.name,
      this.email,
      this.countryCode,
      this.mobile,
      this.countryId,
      this.stateId,
      this.cityId,
      this.packageId,
      this.coverImage,
      this.isTechnician,
      this.hasPendingRequest,
      this.grade,
      this.isVerified,
      this.countryText,
      this.stateText,
      this.cityText});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    countryCode = json['country_code'];
    mobile = json['mobile'];
    countryId = json['country_id'];
    stateId = json['state_id'];
    cityId = json['city_id'];
    packageId = json['package_id'];
    coverImage = json['cover_image'];
    isTechnician = json['is_technician'];
    hasPendingRequest = json['has_pending_request'];
    grade = json['grade'];
    isVerified = json['is_verified'];
    countryText = json['country_text'];
    stateText = json['state_text'];
    cityText = json['city_text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['country_code'] = this.countryCode;
    data['mobile'] = this.mobile;
    data['country_id'] = this.countryId;
    data['state_id'] = this.stateId;
    data['city_id'] = this.cityId;
    data['package_id'] = this.packageId;
    data['cover_image'] = this.coverImage;
    data['is_technician'] = this.isTechnician;
    data['has_pending_request'] = this.hasPendingRequest;
    data['grade'] = this.grade;
    data['is_verified'] = this.isVerified;
    data['country_text'] = this.countryText;
    data['state_text'] = this.stateText;
    data['city_text'] = this.cityText;
    return data;
  }
}
