// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'states_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StatesModel _$StatesModelFromJson(Map<String, dynamic> json) => StatesModel(
      id: json['id'] as int?,
      name: json['name'] as String?,
      title: json['title'] as String?,
    );

Map<String, dynamic> _$StatesModelToJson(StatesModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'title': instance.title,
    };
