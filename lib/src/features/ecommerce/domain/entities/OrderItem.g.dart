// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'OrderItem.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderItem _$OrderItemFromJson(Map<String, dynamic> json) => OrderItem(
      id: json['id'] as int,
      itemsList: (json['items_list'] as List<dynamic>?)
          ?.map((e) => OrderProduct.fromJson(e))
          .toList(),
      trackingUrl: json['tracking_url'] as String?,
      isCancellable: json['is_cancellable'] as bool?,
      itemsThumbnails: (json['items_thumbnails'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      orderMethod: json['order_method'] as String?,
      paymentMethod: json['payment_method'] as String?,
      shippingStatusText: json['shipping_status_text'] as String?,
      totalText: json['total_text'] as String,
      uuid: json['uuid'] as String?,
      feesText: json['fees_text'] as String?,
      subtotaltext: json['subtotaltext'] as String?,
      createdAt: json['created_at'] as String?,
      status: json['status'] as String?,
    );

Map<String, dynamic> _$OrderItemToJson(OrderItem instance) => <String, dynamic>{
      'id': instance.id,
      'uuid': instance.uuid,
      'payment_method': instance.paymentMethod,
      'order_method': instance.orderMethod,
      'total_text': instance.totalText,
      'shipping_status_text': instance.shippingStatusText,
      'items_thumbnails': instance.itemsThumbnails,
      'status': instance.status,
      'created_at': instance.createdAt,
      'items_list': instance.itemsList,
      'is_cancellable': instance.isCancellable,
      'fees_text': instance.feesText,
      'subtotaltext': instance.subtotaltext,
      'tracking_url': instance.trackingUrl,
    };
