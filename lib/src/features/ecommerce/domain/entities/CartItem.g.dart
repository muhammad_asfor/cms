// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'CartItem.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CartItem _$CartItemFromJson(Map<String, dynamic> json) => CartItem(
      id: json['id'] as int,
      totalText: json['total_text'] as String?,
      optionsData: (json['options_data'] as List<dynamic>?)
          ?.map((e) => OptionData.fromJson(e))
          .toList(),
      title: json['title'] as String,
      coverImage: json['cover_image'] as String,
      qty: json['qty'] as int,
      priceText: json['price_text'] as String,
    );

Map<String, dynamic> _$CartItemToJson(CartItem instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'qty': instance.qty,
      'price_text': instance.priceText,
      'total_text': instance.totalText,
      'cover_image': instance.coverImage,
      'options_data': instance.optionsData,
    };
