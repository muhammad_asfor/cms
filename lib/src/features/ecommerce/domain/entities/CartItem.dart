import 'package:json_annotation/json_annotation.dart';
import 'package:progiom_cms/src/features/core/SharedEntities/option_data.dart';

part 'CartItem.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class CartItem {
  final int id;
  final String title;
  int qty;
  final String priceText;
  final String? totalText;
  final String coverImage;
  final List<OptionData>? optionsData;
  CartItem({
    required this.id,
    this.totalText,
    this.optionsData,
    required this.title,
    required this.coverImage,
    required this.qty,
    required this.priceText,
  });

  factory CartItem.fromJson(json) => _$CartItemFromJson(json);
  toJson() => _$CartItemToJson(this);

  static List<CartItem> fromJsonList(List json) {
    return json.map((e) => CartItem.fromJson(e)).toList();
  }
}
