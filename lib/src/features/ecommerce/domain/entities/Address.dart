import 'package:json_annotation/json_annotation.dart';

part 'Address.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class Address {
  final int? userId;
  final String name;
  final String? nickname;
  final String? description;
  final int? countryId;
  final String? createdAt;
  final int id;
  final int? sorting;
  final int? cityId;
  final String? countryText;
  final String? cityText;
  final int? isDefault;

  Address(
      {this.userId,
      required this.name,
      this.nickname,
      this.description,
      this.countryId,
      this.createdAt,
      required this.id,
      this.sorting,
      this.cityId,
      this.countryText,
      this.cityText,
      this.isDefault});

  factory Address.fromJson(json) => _$AddressFromJson(json);
  toJson() => _$AddressToJson(this);

  static List<Address> fromJsonList(List json) {
    return json.map((e) => Address.fromJson(e)).toList();
  }
}
