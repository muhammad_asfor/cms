import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'cityModel.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class CityModel {
  final int? id;
  final String? name;

  final String? title;

  CityModel({
    this.id,
    this.name,
    this.title,
  });

  factory CityModel.fromJson(json) => _$CityModelFromJson(json);
  toJson() => _$CityModelToJson(this);

  static List<CityModel> fromJsonList(List json) {
    return json.map((e) => CityModel.fromJson(e)).toList();
  }

  CityModel copyWith({
    int? id,
    String? name,
    String? title,
  }) {
    return CityModel(
      id: id ?? this.id,
      name: name ?? this.name,
      title: title ?? this.title,
    );
  }

  @override
  String toString() => 'CityModel(id: $id, name: $name, title: $title)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is CityModel &&
        other.id == id &&
        other.name == name &&
        other.title == title;
  }

  @override
  int get hashCode => id.hashCode ^ name.hashCode ^ title.hashCode;
}
