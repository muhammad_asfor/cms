import 'package:json_annotation/json_annotation.dart';

part 'CountryModel.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class CountryModel {
  final int id;
  final String? name;
  final String? phonecode;
  final String? currency;
  final String? emoji;
  final List<CountryTranslation>? translations;
  final int? status;

  final String? title;

  CountryModel(
      {required this.id,
      this.name,
      this.phonecode,
      this.currency,
      this.emoji,
      this.translations,
      this.status,
      this.title});

  factory CountryModel.fromJson(json) => _$CountryModelFromJson(json);
  toJson() => _$CountryModelToJson(this);

  static List<CountryModel> fromJsonList(List json) {
    return json.map((e) => CountryModel.fromJson(e)).toList();
  }
}

@JsonSerializable(fieldRename: FieldRename.snake)
class CountryTranslation {
  final int? id;
  final int? countryId;
  final String? locale;
  final String? title;
  final String? capital;

  CountryTranslation(
      {this.id, this.countryId, this.locale, this.title, this.capital});

  factory CountryTranslation.fromJson(json) =>
      _$CountryTranslationFromJson(json);
  toJson() => _$CountryTranslationToJson(this);

  static List<CountryTranslation> fromJsonList(List json) {
    return json.map((e) => CountryTranslation.fromJson(e)).toList();
  }
}
