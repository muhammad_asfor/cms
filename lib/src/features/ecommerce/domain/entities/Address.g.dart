// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Address.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Address _$AddressFromJson(Map<String, dynamic> json) => Address(
      userId: json['user_id'] as int?,
      name: json['name'] as String,
      nickname: json['nickname'] as String?,
      description: json['description'] as String?,
      countryId: json['country_id'] as int?,
      createdAt: json['created_at'] as String?,
      id: json['id'] as int,
      sorting: json['sorting'] as int?,
      cityId: json['city_id'] as int?,
      countryText: json['country_text'] as String?,
      cityText: json['city_text'] as String?,
      isDefault: json['is_default'] as int?,
    );

Map<String, dynamic> _$AddressToJson(Address instance) => <String, dynamic>{
      'user_id': instance.userId,
      'name': instance.name,
      'nickname': instance.nickname,
      'description': instance.description,
      'country_id': instance.countryId,
      'created_at': instance.createdAt,
      'id': instance.id,
      'sorting': instance.sorting,
      'city_id': instance.cityId,
      'country_text': instance.countryText,
      'city_text': instance.cityText,
      'is_default': instance.isDefault,
    };
