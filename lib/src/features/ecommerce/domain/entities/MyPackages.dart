class MyPackages {
  bool? success;
  String? message;
  Data? data;

  MyPackages({this.success, this.message, this.data});

  MyPackages.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  int? id;
  String? uuid;
  int? userId;
  int? packageId;
  Null? paymentId;
  Null? comment;
  String? status;
  String? startDate;
  String? endDate;
  String? createdAt;
  String? updatedAt;
  bool? isActive;
  int? remainingDays;
  String? package_name;

  Data(
      {this.id,
      this.uuid,
      this.userId,
      this.packageId,
      this.paymentId,
      this.comment,
      this.status,
      this.startDate,
      this.endDate,
      this.createdAt,
      this.updatedAt,
      this.isActive,
      this.package_name,
      this.remainingDays});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    uuid = json['uuid'];
    userId = json['user_id'];
    packageId = json['package_id'];
    paymentId = json['payment_id'];
    comment = json['comment'];
    status = json['status'];
    startDate = json['start_date'];
    endDate = json['end_date'];
    createdAt = json['created_at'];
    package_name = json['package_name'];
    updatedAt = json['updated_at'];
    isActive = json['is_active'];
    remainingDays = json['remaining_days'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['uuid'] = this.uuid;
    data['user_id'] = this.userId;
    data['package_id'] = this.packageId;
    data['payment_id'] = this.paymentId;
    data['comment'] = this.comment;
    data['status'] = this.status;
    data['start_date'] = this.startDate;
    data['end_date'] = this.endDate;
    data['package_name'] = this.package_name;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['is_active'] = this.isActive;
    data['remaining_days'] = this.remainingDays;
    return data;
  }
}
