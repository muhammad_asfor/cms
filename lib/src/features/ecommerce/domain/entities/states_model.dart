import 'package:json_annotation/json_annotation.dart';

part 'states_model.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class StatesModel {
  final int? id;
  final String? name;

  final String? title;

  StatesModel({
    this.id,
    this.name,
    this.title,
  });

  factory StatesModel.fromJson(json) => _$StatesModelFromJson(json);
  toJson() => _$StatesModelToJson(this);

  static List<StatesModel> fromJsonList(List json) {
    return json.map((e) => StatesModel.fromJson(e)).toList();
  }

  StatesModel copyWith({
    int? id,
    String? name,
    String? title,
  }) {
    return StatesModel(
      id: id ?? this.id,
      name: name ?? this.name,
      title: title ?? this.title,
    );
  }

  @override
  String toString() => 'StatesModel(id: $id, name: $name, title: $title)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is StatesModel &&
        other.id == id &&
        other.name == name &&
        other.title == title;
  }

  @override
  int get hashCode => id.hashCode ^ name.hashCode ^ title.hashCode;
}
