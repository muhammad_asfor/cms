// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'points_registry.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PointsRegistry _$PointsRegistryFromJson(Map<String, dynamic> json) =>
    PointsRegistry(
      user: User.fromJson(json['user']),
      type: json['type'] as String?,
      points: json['points'] as String?,
      date: json['date'] as String?,
    );

Map<String, dynamic> _$PointsRegistryToJson(PointsRegistry instance) =>
    <String, dynamic>{
      'user': instance.user,
      'date': instance.date,
      'points': instance.points,
      'type': instance.type,
    };
