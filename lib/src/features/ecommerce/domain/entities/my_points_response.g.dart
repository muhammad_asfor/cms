// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'my_points_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MyPointsResponse _$MyPointsResponseFromJson(Map<String, dynamic> json) =>
    MyPointsResponse(
      currentPoints: json['current_points'] as int?,
      totalPoints: json['total_points'] as int?,
      totalSaving: json['total_saving'] as String?,
      usedPoints: json['used_points'] as int?,
      message: json['message'] as String?,
    );

Map<String, dynamic> _$MyPointsResponseToJson(MyPointsResponse instance) =>
    <String, dynamic>{
      'current_points': instance.currentPoints,
      'total_points': instance.totalPoints,
      'used_points': instance.usedPoints,
      'total_saving': instance.totalSaving,
      'message': instance.message,
    };
