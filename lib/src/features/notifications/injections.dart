import 'package:progiom_cms/Getit_instance.dart';
import 'domain/repositories/NotificationsRepository.dart';

import 'data/datasources/NotificationsApi.dart';
import 'data/repositories/NotificationRepositoryImpl.dart';

// depend on shredpreferences ans dio.
void initNotificationsInjections() async {
  sl.registerLazySingleton<NotificationsApi>(() => NotificationsApi());

  sl.registerLazySingleton<NotificationsRepository>(
      () => NotificationsRepositoryImpl(
            sl(),
          ));
}
