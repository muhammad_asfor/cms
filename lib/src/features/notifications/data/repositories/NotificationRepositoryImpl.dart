import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/src/features/notifications/data/datasources/NotificationsApi.dart';
import 'package:progiom_cms/src/features/notifications/domain/entities/ServerNotification.dart';
import 'package:progiom_cms/src/features/core/util/error/failures.dart';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/notifications/domain/repositories/NotificationsRepository.dart';

class NotificationsRepositoryImpl extends NotificationsRepository {
  final NotificationsApi notifiationsApi;
  NotificationsRepositoryImpl(this.notifiationsApi);
  @override
  Future<Either<Failure, List<ServerNotification>>> getMyNotifications(
      int page) async {
    try {
      final result = await notifiationsApi.getMyNotifications(page: page);
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, bool>> setNotifiationAsRead(
      String notificationId) async {
    try {
      final result = await notifiationsApi.setNotificationAsRead(
          notificationId: notificationId);
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, bool>> disableEnableNotification(
      {required bool enable}) async {
    try {
      final result =
          await notifiationsApi.disableEnableNotification(enable: enable);
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, bool>> disableEnableOffersNotification(
      {required bool enable}) async {
    try {
      final result =
          await notifiationsApi.disableEnableOffersNotification(enable: enable);
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, bool>> disableEnableDiscountNotification(
      {required bool enable}) async {
    try {
      final result =
          await notifiationsApi.disableEnableDiscountNotification(enable: enable);
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }
}
