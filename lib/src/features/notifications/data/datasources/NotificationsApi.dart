import 'package:dio/dio.dart';
import 'package:progiom_cms/src/features/core/network/dio_error_handler.dart';
import 'package:progiom_cms/src/features/core/util/error/exceptions.dart';
import 'package:progiom_cms/src/features/notifications/domain/entities/ServerNotification.dart';

import '../../../../../Getit_instance.dart';

class NotificationsApi {
  Future<List<ServerNotification>> getMyNotifications(
      {required int page}) async {
    try {
      return ServerNotification.fromJsonList(
          (await sl<Dio>().get("api/notifications?page=$page")).data["data"]);
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<bool> setNotificationAsRead({required String notificationId}) async {
    try {
      await sl<Dio>().get("api/notifications/$notificationId");
      return true;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<bool> disableEnableNotification({required bool enable}) async {
    try {
      await sl<Dio>().post("api/preferences/push_notifications",
          data: {"_method": "PUT", "value": enable ? "1" : "0"});
      return true;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<bool> disableEnableOffersNotification({required bool enable}) async {
    try {
      await sl<Dio>().post("api/preferences/offers_notifications",
          data: {"_method": "PUT", "value": enable ? "1" : "0"});
      return true;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  Future<bool> disableEnableDiscountNotification({required bool enable}) async {
    try {
      await sl<Dio>().post("api/preferences/discount_notifications",
          data: {"_method": "PUT", "value": enable ? "1" : "0"});
      return true;
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }
}
