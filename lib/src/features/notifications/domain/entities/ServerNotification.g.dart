// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ServerNotification.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ServerNotification _$ServerNotificationFromJson(Map<String, dynamic> json) =>
    ServerNotification(
      id: json['id'] as String,
      createdAt: json['created_at'] as String,
      notificationTitle: json['notification_title'] as String,
      notificationText: json['notification_text'] as String,
      notificationType: json['notification_type'] as String,
      isRead: json['is_read'] as bool,
      isSeen: json['is_seen'] as bool,
      entityType: json['entity_type'] as String?,
      entityId: json['entity_id'] as int?,
      link: json['link'] as String?,
      image: json['image'] as String?,
    );

Map<String, dynamic> _$ServerNotificationToJson(ServerNotification instance) =>
    <String, dynamic>{
      'id': instance.id,
      'created_at': instance.createdAt,
      'notification_title': instance.notificationTitle,
      'notification_text': instance.notificationText,
      'notification_type': instance.notificationType,
      'is_read': instance.isRead,
      'is_seen': instance.isSeen,
      'entity_type': instance.entityType,
      'entity_id': instance.entityId,
      'link': instance.link,
      'image': instance.image,
    };
