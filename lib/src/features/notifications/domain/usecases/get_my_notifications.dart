import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/notifications/domain/entities/ServerNotification.dart';
import 'package:progiom_cms/src/features/notifications/domain/repositories/NotificationsRepository.dart';

class GetMyNotifications
    extends UseCase<List<ServerNotification>, GetMyNotificationsParams> {
  final NotificationsRepository repository;
  GetMyNotifications(this.repository);

  @override
  Future<Either<Failure, List<ServerNotification>>> call(
      GetMyNotificationsParams params) async {
    return await repository.getMyNotifications(
      params.page,
    );
  }
}

class GetMyNotificationsParams {
  final int page;
  GetMyNotificationsParams({required this.page});
}
