import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/notifications/domain/repositories/NotificationsRepository.dart';

class DisableEnableDiscountNotification
    extends UseCase<bool, DisableEnableDiscountNotificationParams> {
  final NotificationsRepository repository;
  DisableEnableDiscountNotification(this.repository);

  @override
  Future<Either<Failure, bool>> call(
      DisableEnableDiscountNotificationParams params) async {
    return await repository.disableEnableDiscountNotification(
        enable: params.enable);
  }
}

class DisableEnableDiscountNotificationParams {
  final bool enable;
  DisableEnableDiscountNotificationParams({required this.enable});
}
