import 'package:progiom_cms/src/features/core/core.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/notifications/domain/repositories/NotificationsRepository.dart';

class DisableEnableOffersNotification
    extends UseCase<bool, DisableEnableOffersNotificationParams> {
  final NotificationsRepository repository;
  DisableEnableOffersNotification(this.repository);

  @override
  Future<Either<Failure, bool>> call(
      DisableEnableOffersNotificationParams params) async {
    return await repository.disableEnableOffersNotification(enable: params.enable);
  }
}

class DisableEnableOffersNotificationParams {
  final bool enable;
  DisableEnableOffersNotificationParams({required this.enable});
}
