import 'package:progiom_cms/Getit_instance.dart';
import 'package:progiom_cms/src/features/rate_app/data/data_sources/rateApp_shared_prefs.dart';
import 'package:progiom_cms/src/features/rate_app/data/repositories/rateApp_repo_empl.dart';
import 'package:progiom_cms/src/features/rate_app/domain/usecases/init_rate_app.dart';

initRateTimeInjections() async {
  sl.registerLazySingleton(() => RateAppSharedPrefs(sl()));
  sl.registerLazySingleton(() => RateAppRepositoryImpl(sl()));
  sl.registerLazySingleton(() => InitRateTime(sl()));
}
