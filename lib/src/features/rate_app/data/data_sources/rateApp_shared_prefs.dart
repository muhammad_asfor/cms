import 'package:shared_preferences/shared_preferences.dart';

import '../../rate_app_constants.dart';

class RateAppSharedPrefs {
  final SharedPreferences _preferences;
  RateAppSharedPrefs(this._preferences);

  Future<bool> register() async {
    return _preferences.setString(
        RateAppConstants.rate_app_time, DateTime.now().toString());
  }

  Future<String?> initRateTime() async {
    return _preferences.getString(RateAppConstants.rate_app_time);
  }

  Future<bool> disableRate() async {
    return _preferences.setString(RateAppConstants.rate_app_time, "0");
  }

  Future<bool> rateLater() async {
    return _preferences.setString(
        RateAppConstants.rate_app_time, DateTime.now().toString());
  }
}
