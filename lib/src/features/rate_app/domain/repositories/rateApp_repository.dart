import 'package:dartz/dartz.dart';

import '../../../../../core.dart';

abstract class RateAppRepository {
  Future<Either<Failure, String?>> initRateTime();
  Future<Either<Failure, bool>> register();
  Future<Either<Failure, bool>> disableRate();
  Future<Either<Failure, bool>> rateLater();
}
