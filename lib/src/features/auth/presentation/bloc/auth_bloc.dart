import 'dart:async';

import 'package:progiom_cms/auth.dart';
import 'package:progiom_cms/src/features/auth/domain/usecases/get_my_profile.dart';
import 'package:progiom_cms/src/features/auth/domain/usecases/social_login.dart';
import 'package:progiom_cms/src/features/core/SharedEntities/User.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:bloc/bloc.dart';

import 'package:meta/meta.dart';
import 'package:progiom_cms/src/features/auth/data/datasources/AuthSharedPref.dart';
import 'package:progiom_cms/src/features/auth/data/datasources/auth_api.dart';
import 'package:progiom_cms/src/features/auth/domain/entities/LogginChecker.dart';
import 'package:progiom_cms/src/features/auth/domain/repositories/auth_repository.dart';
import 'package:progiom_cms/src/features/auth/domain/usecases/check_if_loggedin.dart';
import 'package:progiom_cms/src/features/auth/domain/usecases/get_public_token.dart';
import 'package:progiom_cms/src/features/auth/domain/usecases/login.dart';
import 'package:progiom_cms/src/features/auth/domain/usecases/logout.dart';
import 'package:progiom_cms/src/features/auth/domain/usecases/signUp.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthBloc(this.sharedPrefs, this.authApi, this.authRepository)
      : super(AuthInitial()) {
    on<LoginEvent>(_onLogin);
    on<InitToken>(_onInitToken);
    on<SignUpEvent>(_onSignUp);
    on<LogoutEvent>(_onLogout);
    on<GetMyProfileEvent>(_onGetMyProfile);
    on<UpdateProfileEvent>(_onUpdateProfile);
    on<LoginSocialEvent>(_onSocialLogin);
  }

  final AuthSharedPrefs sharedPrefs;
  final AuthApi authApi;
  late final AuthRepository authRepository;

  bool isGuest = true;
  bool isFirstLaunch = true;

  void _onLogin(LoginEvent event, Emitter<AuthState> emit) async {
    emit(LoadingLogin());
    final login = Login(authRepository);

    final result = await login.call(LoginParams(
      email: event.email,
      password: event.password,
    ));

    result.fold((l) {
      emit(ErrorInLogin(l.errorMessage));
    }, (r) {
      isGuest = false;
      emit(LoginSuccess(r));
    });
  }

  _onSocialLogin(LoginSocialEvent event, Emitter<AuthState> emit) async {
    emit(LoadingLogin());

    final loginResult = await SocialLogin(authRepository)
        .call(SocialLoginParams(provider: event.provider, token: event.token));
    loginResult.fold((l) {
      emit(ErrorInLogin(l.errorMessage));
    }, (r) {
      isGuest = false;
      emit(LoginSuccess(r));
    });
  }

  Future<void> _onUpdateProfile(
      UpdateProfileEvent event, Emitter<AuthState> emit) async {
    emit(LoadingUpdateProfile());
    final updateProfile = UpdateProfile(authRepository);
    final result = await updateProfile.call(UpdateProfileParams(event.data));
    result.fold((l) {
      emit(ErrorInUpdateProfile(l.errorMessage));
    }, (r) {
      emit(ProfileUpdated());
    });
  }

  _onGetMyProfile(GetMyProfileEvent event, Emitter<AuthState> emit) async {
    emit(LoadingProfile());
    final getMyProfile = GetMyProfile(authRepository);
    final result = await getMyProfile.call(NoParams());
    result.fold((l) {
      emit(ErrorInProfile(l.errorMessage));
    }, (r) {
      emit(ProfileReady(r));
    });
  }

  _onLogout(LogoutEvent event, Emitter<AuthState> emit) async {
    Logout logout = Logout(authRepository);
    var res = await logout.call(NoParams());
    res.fold((l) {}, (r) {
      add(InitToken(isAfterLogout: true));
    });
  }

  Future<void> _onSignUp(SignUpEvent event, Emitter<AuthState> emit) async {
    emit(LoadingSignUp());
    final signUp = SignUp(authRepository);

    final result = await signUp.call(SignupParams(user: event.user));

    result.fold((l) {
      emit(ErrorSignUp(l.errorMessage));
    }, (r) {
      isGuest = false;
      emit(SignUpSuccess(r));
    });
  }

  Future<void> _onInitToken(InitToken event, Emitter<AuthState> emit) async {
    final checker = await checkLogginStatus();

    if (checker.token != null) {
      emit(PublicTokenReady(event.isAfterLogout, token: checker.token));
    } else {
      final getToken = GetPublicToken(authRepository);

      final result = await getToken.call(NoParams());

      result.fold((l) {
        emit(ErrorInGettingPublicToken(l.errorMessage));
      }, (r) {
        emit(PublicTokenReady(event.isAfterLogout));
      });
    }
  }

  Future<LogginChecker> checkLogginStatus() async {
    final CheckIfLoggedin checkIfLoggedin = CheckIfLoggedin(authRepository);
    final result = await checkIfLoggedin.call(NoParams());
    LogginChecker? logginChecker;
    result.fold((l) {
      // WILL NEVER HAPPEN
    }, (r) {
      isFirstLaunch = r.isFirstLaunch;
      isGuest = !r.isLoggedIn;
      logginChecker = r;
    });
    return logginChecker!;
  }
}
