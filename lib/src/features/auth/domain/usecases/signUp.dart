import 'package:progiom_cms/src/features/auth/domain/repositories/auth_repository.dart';
import 'package:progiom_cms/src/features/core/SharedEntities/User.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';

import 'package:equatable/equatable.dart';
import 'package:dartz/dartz.dart';

class SignUp extends UseCase<User, SignupParams> {
  final AuthRepository repository;
  SignUp(this.repository);
  @override
  Future<Either<Failure, User>> call(SignupParams params) async {
    return await repository.signup(user: params.user);
  }
}

class SignupParams extends Equatable {
  final Map<String, dynamic> user;

  SignupParams({required this.user});

  @override
  List<Object?> get props => [user];
}
