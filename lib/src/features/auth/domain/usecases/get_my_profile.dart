import 'package:progiom_cms/src/features/auth/domain/repositories/auth_repository.dart';
import 'package:progiom_cms/src/features/core/core.dart';

import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:dartz/dartz.dart';

class GetMyProfile extends UseCase<User, NoParams> {
  final AuthRepository repository;
  GetMyProfile(this.repository);

  @override
  Future<Either<Failure,User>> call(NoParams params) async {
    return await repository.getMyProfile();
  }
}
