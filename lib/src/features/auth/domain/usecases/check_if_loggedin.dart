

import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/auth/domain/entities/LogginChecker.dart';
import 'package:progiom_cms/src/features/auth/domain/repositories/auth_repository.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
class CheckIfLoggedin extends UseCase<LogginChecker, NoParams> {
  final AuthRepository repository;
  CheckIfLoggedin(this.repository);
  // failure mean there is no user logged in, else we return the access_token.
  @override
  Future<Either<Failure, LogginChecker>> call(NoParams params) async {
    return await repository.checkIfLoggedin();
  }
}
