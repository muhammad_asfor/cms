import 'package:equatable/equatable.dart';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/src/features/auth/domain/repositories/auth_repository.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';

class ChangePassword extends UseCase<bool, ChangePasswordParams> {
  final AuthRepository repository;
  ChangePassword(this.repository);
  @override
  Future<Either<Failure, bool>> call(ChangePasswordParams params) async {
    return await repository.changePassword(
        email: params.email,
        password: params.password,
        resetCode: params.resetCode);
  }
}

class ChangePasswordParams extends Equatable {
  final String email;
  final String password;
  final String resetCode;

  ChangePasswordParams(
      {required this.email, required this.password, required this.resetCode});

  @override
  List<Object?> get props => [email, password, resetCode];
}
