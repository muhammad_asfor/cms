import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:progiom_cms/src/features/auth/domain/entities/LogginChecker.dart';
import 'package:progiom_cms/src/features/core/SharedEntities/Token.dart';
import 'package:progiom_cms/src/features/core/SharedEntities/User.dart';

import 'package:progiom_cms/src/features/core/util/util.dart';

abstract class AuthRepository {
  Future<Either<Failure, User>> signup({required Map<String, dynamic> user});

  Future<Either<Failure, User>> login(
      {required String email, required String password, Dio? tokenDio});

  Future<Either<Failure, User>> socialLogin(
      {required String token, required String provider});
  Future<Either<Failure, void>> updateProfile(Map<String, dynamic> data);
  Future<Either<Failure, User>> getMyProfile();

  Future<Either<Failure, void>> logout();

  Future<Either<Failure, void>> setFcmToken({required String token});
  Future<Either<Failure, String>> refreashToken(
      {required String refreashToken});

  Future<Either<Failure, bool>> requestResetPassword({required String email});
  Future<Either<Failure, bool>> changePassword(
      {required String email,
      required String password,
      required String resetCode});

  Future<Either<Failure, bool>> validateResetCode(
      {required String code, required String email});
  Future<Either<Failure, LogginChecker>> checkIfLoggedin();

  Future<Either<Failure, Token>> getPublicToken();
}
