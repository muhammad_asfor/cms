class uplodeImage {
  String? url;
  String? filename;

  uplodeImage({this.url, this.filename});

  uplodeImage.fromJson(Map<String, dynamic> json) {
    url = json['url'];
    filename = json['filename'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['url'] = this.url;
    data['filename'] = this.filename;
    return data;
  }
}
