import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:progiom_cms/Getit_instance.dart';
import 'package:progiom_cms/src/features/auth/data/datasources/auth_api.dart';
import 'package:progiom_cms/src/features/auth/data/repositories/auth_repository_impl.dart';
import 'package:progiom_cms/src/features/auth/domain/repositories/auth_repository.dart';
import 'package:progiom_cms/src/features/auth/presentation/bloc/auth_bloc.dart';
import 'package:progiom_cms/src/features/auth/settings.dart';
import 'package:progiom_cms/src/features/core/injection/injections.dart';
import 'package:progiom_cms/src/features/core/network/network.dart';
import 'package:progiom_cms/src/features/core/util/util.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'data/datasources/AuthSharedPref.dart';

// depend on shredPreferences.
Future initAuthInjection(
    String BaseUrl, SecretModel secrets, Function() navigateLoginCall) async {
  initRootLogger();

  sl.registerSingletonAsync<Dio>(() async {
    final dio = Dio(BaseOptions(
        baseUrl: BaseUrl,
        validateStatus: (s) {
          return s! < 300;
        },
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          "charset": "utf-8",
          "Accept-Charset": "utf-8",
          "Accept-Language":
              sl<SharedPreferences>().getString(AuthConstants.Language) ?? "ar",
          "currency":
              sl<SharedPreferences>().getString(AuthConstants.Currency) ?? "",
        },
        responseType: ResponseType.json));

    dio.interceptors.add(LoggerInterceptor(
      logger,
      request: true,
      requestBody: true,
      error: true,
      responseBody: true,
      responseHeader: false,
      requestHeader: true,
    ));
    return dio;
  });
  await sl.isReady<Dio>();
  sl.registerSingleton<AuthSharedPrefs>(AuthSharedPrefs(sl()));
  sl.registerSingleton<AuthApi>(AuthApi());
  sl.registerSingleton<AuthRepository>(AuthRepositoryImpl(sl(), sl()));

  AuthSettings.init(
      secrets.ClientPersonalSecret,
      secrets.ClientPersonalId,
      secrets.ClientPasswordSecret,
      secrets.ClientPasswordId,
      navigateLoginCall);
  sl.registerSingleton<AuthBloc>(AuthBloc(sl(), sl(), sl()));
}
