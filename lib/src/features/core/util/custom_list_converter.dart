import 'package:json_annotation/json_annotation.dart';

class CustomListConverter implements JsonConverter<dynamic, List<String>?> {
  const CustomListConverter();

  @override
  List<String>? fromJson(dynamic json) {
    print('json is $json');
    List<String>? value;
    if (json == null) {
      return null;
    } else if (json is List<String>) {
      value = json;
    } else {
      value = [json];
    }
    return value;
  }

  @override
  List<String>? toJson(json) => [json];
}
