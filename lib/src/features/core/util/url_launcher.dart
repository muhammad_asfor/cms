import 'dart:io';

import 'package:url_launcher/url_launcher.dart';

openWhatsapp(String phone) async {
  var whatsappUrl;
  if (Platform.isAndroid) {
    whatsappUrl = "whatsapp://send?phone=${phone}";
  } else {
    whatsappUrl = "https://api.whatsapp.com/send?phone=$phone }"; // new line

  }
  try {
    launch(whatsappUrl);
  } catch (e) {}
}

sendMessageToWhatsapp(String phone, String message) async {
  var whatsappUrl;
  if (Platform.isAndroid) {
    whatsappUrl = "whatsapp://send?phone=${phone}" +
        "&text=${Uri.encodeComponent(message)}";
  } else {
    whatsappUrl = "whatsapp://send?phone=${phone}" +
        "&text=${Uri.encodeComponent(message)}";

    return "https://api.whatsapp.com/send?phone=$phone&text=${Uri.encodeComponent(message)}"; // new line

  }
  try {
    launch(whatsappUrl);
  } catch (e) {
    throw 'Could not launch ${whatsappUrl}';
  }
}
