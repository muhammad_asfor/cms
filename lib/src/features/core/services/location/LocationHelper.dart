// import 'package:flutter/services.dart';
// import 'package:location/location.dart';

// class LocationHelper {
//   final Function onSuccess;
//   final Function onFailure;
//   LocationHelper(this.onSuccess, this.onFailure) {
//     _location = Location();
//   }

//   Location _location;
//   bool _serviceEnabled = false;
//   Future<void> initLocation() async {
//     await _checkPermissions();
//     if (_permissionGranted != PermissionStatus.granted) {
//       await _requestPermission();
//     }
//     if (_permissionGranted == PermissionStatus.granted) {
//       _serviceEnabled = await _location.serviceEnabled();
//       if (!_serviceEnabled) {
//         await _requestService();
//       }
//       if (_serviceEnabled) {
//         _getLocation();
//       } else {
//         onFailure(S.current.permissionNotGranted);
//       }
//     } else {
//       onFailure(S.current.permissionNotGranted);
//     }
//   }

//   Future<void> _requestService() async {
//     final bool serviceRequestedResult = await _location.requestService();

//     _serviceEnabled = serviceRequestedResult;
//     if (_serviceEnabled) {
//       initLocation();
//     }
//   }

//   PermissionStatus _permissionGranted;

//   Future<void> _checkPermissions() async {
//     final PermissionStatus permissionGrantedResult =
//         await _location.hasPermission();

//     _permissionGranted = permissionGrantedResult;
//   }

//   Future<void> _requestPermission() async {
//     if (_permissionGranted != PermissionStatus.granted) {
//       final PermissionStatus permissionRequestedResult =
//           await _location.requestPermission();

//       _permissionGranted = permissionRequestedResult;

//       if (permissionRequestedResult != PermissionStatus.granted) {
//         return;
//       }
//     }
//   }

//   Future<void> _getLocation() async {
//     try {
//       final LocationData _locationResult = await _location.getLocation();
//       if (_locationResult.latitude != null) {
//         onSuccess(_locationResult.longitude, _locationResult.latitude);
//       }
//     } on PlatformException catch (err) {
//       onFailure(err);
//     }
//   }
// }
