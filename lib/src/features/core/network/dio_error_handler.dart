import 'package:dio/dio.dart';

import 'package:progiom_cms/Getit_instance.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../auth.dart';

String handleDioError(DioError error) {
  String errorDescription = "";

  switch (error.type) {
    case DioErrorType.cancel:
      errorDescription =
          sl<SharedPreferences>().getString(AuthConstants.Language) != "en"
              ? "تم إلغاء الطلب إلى الخادم"
              : "Request to API server was cancelled";
      break;
    case DioErrorType.connectTimeout:
      errorDescription =
          sl<SharedPreferences>().getString(AuthConstants.Language) != "en"
              ? "انتهت مهلة الاتصال بالخادم"
              : "Connection timeout with API server";
      break;
    case DioErrorType.other:
      errorDescription =
          sl<SharedPreferences>().getString(AuthConstants.Language) != "en"
              ? "مشكلة في الاتصال بالإنترنت."
              : "Internet Connection Problem.";
      break;
    case DioErrorType.receiveTimeout:
      errorDescription =
          sl<SharedPreferences>().getString(AuthConstants.Language) != "en"
              ? "تلقي المهلة فيما يتعلق بالخادم"
              : "Receive timeout in connection with API server";
      break;
    case DioErrorType.response:
      {
        // if (error.response.statusCode == 403) {
        //   errorDescription = S.current.dontHavePermission;
        //   AppSnackBar.show(null, S.current.dontHavePermission, ToastType.Error);
        // } else

        errorDescription =
            sl<SharedPreferences>().getString(AuthConstants.Language) != "en"
                ? "تم استلام رمز حالة غير صالح: ${error.response?.statusCode}"
                : "Received invalid status code: ${error.response?.statusCode}";
        break;
      }

    case DioErrorType.sendTimeout:
      errorDescription =
          sl<SharedPreferences>().getString(AuthConstants.Language) != "en"
              ? "إرسال المهلة فيما يتعلق بالخادم"
              : "Send timeout in connection with API server";
      break;
  }

  return errorDescription;
}
