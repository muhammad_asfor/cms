import 'package:json_annotation/json_annotation.dart';
part 'option_data.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class OptionData {
  final int optionId;

  final Map<String, dynamic> allowedOptions;

  final String? title;
  final String? selectedOption;
  OptionData(
      {required this.title,
      required this.optionId,
      this.selectedOption,
      required this.allowedOptions});

  factory OptionData.fromJson(json) => _$OptionDataFromJson(json);
  toJson() => _$OptionDataToJson(this);
}
