import 'package:json_annotation/json_annotation.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/src/features/core/SharedEntities/dynamic_field.dart';
part 'service.g.dart';

/// For Batal
@JsonSerializable(fieldRename: FieldRename.snake)
class Service {
  final int id;
  final int? parentId;
  final int? featured;
  final String coverImage;
  final String title;
  final String? categoryText;
  final User? user;
  // Null parent;

  Service({
    required this.id,
    this.parentId,
    this.featured,
    this.categoryText,
    required this.coverImage,
    required this.title,
    this.user,
  });

  factory Service.fromJson(json) => _$ServiceFromJson(json);
  toJson() => _$ServiceToJson(this);
}
